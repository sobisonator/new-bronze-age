﻿maganite = {	
	color = hsv { 0.43 0.1 0.55 }

	primary = light_infantry
	second = camels
	flank = archers
 
	primary_navy = tetrere
	secondary_navy = octere
	flank_navy = liburnian
	
	levy_template = levy_arabs

	male_names = {
		Jushur
		Kullassina-bel
		Nangishlishma
		En-tarah-ana
		Babum
		Puannum
		Kalibum
		Kulumum
		Zuqaqip
		Atab
		A-ba
		Mashda
		Arwium
		Etana
		Balih
		En-me-nuna
		Melem-kish
		Barsal-nuna
		Zamug
		Tizqar
		Ilku
		En-me-barage-si
		Iltasadum
		Aga
		Susuda
		Dadasig
		Mamagal
		Klbum
		Tuge
		Men-nuna
		Enbi-Ishtar
		Lugalngu
		Anbu
		Anba
		Bazi
		Zizi
		Limer
		Sharrum-Iter
		Kug-Bau
		Kubaba
		Sargon
		Rimush
		Manishtushu
		Manishtusu
		Naram-Sin
		Shar-kali-sharri
		Irgigi
		Imi
		Nanum
		Ilulu
		Dudu
		Shu-Durul
		Tudiya
		Adamu
		Yangi
		Suhlamu
		Harharhu
		Mandaru
		Imsu
		Harsu
		Didanu
		Hana
		Zuabu
		Nuabu
		Abazu
		Belu
		Azarah
		Ushpia
		Apiashal
		Hale
		Samani
		Hayani
		Ilu-Mer
		Yakmesi
		Yakmeni
		Yazkur-el
		Ila-kabkabu
		Aminu
		Sulili
		Kikkla
		Akiya
		Puzur-Ashur
		Shalim-ahum
		Ilu-shuma
		Erishum
		Ikunum
		Sargon
		Naram-Sin
		Shamshi-Adad
		Imshe-Dagan
		Mut-Ashkur
		Rimush
		Asinum
		Ashur-dugul
		Ashur-apla-idi
		Nasir-Sin
		Sin-nami
		Ipqi-Ishtar
		Adad-salulu
		Adasi
		Bel-bani
		Libaya
		Sharma-Adad
		Iptar-Sin
		Bazaya
		Lullaya
		Ashur-nirari
		Enlil-nsir
		Nur-ili
		Ashur-shaduni
		Ashur-rabi
		Ashur-nadin-ahhe
		Ashur-bel-nisheshu
		Eriba-Adad
		Ashur-uballit
		Enlil-nirari
		Arik-den-ili
		Adad-nirari
		Shalmaneser
		Tukulti-Ninurta
		Ashur-nadin-apli
		Enlil-kudurri-usur
		Ninurta-apal-Ekur
		Ashur-dan
		Ninurta-tukulti-Ashur
		Mutakkil-nusku
		Ashur-resh-ishi
		Tiglath-Pileser
		Asharid-apal-Ekur
		Ashur-bel-kala
		Ashurnasirpal
		Sennacherib
		Esarhaddon
		Ashurbanipal
		Ashur-etil-ilani
		Sinharishkun
		Sin-shumu-lishir
	}

	female_names = {
		Demeetheresu
		Nigsummulugal
		Deemethereesu
		Niiqarquusu
		Manishtusu
		Seluku
		Nur-Ayya
		Amata
		Belessunu
		Arahunaa
		Ahatsunu
		Ubalnu
		Gemeti
		Ettu
		Zakiti
		Humusi
		Alittum
		Arwia
		Kullaa
		Mushezibti
		Nidintu
		Enheduana
	}
	family = {
		Ahmid.Ahmid.Ahmid.Ahmid
		Galestus.Galestid.Galestid.Galestid
		Kenamus.Kenamid.Kenamid.Kenamid
		Naravid.Naravid.Naravid.Naravid
		Penamus.Penamid.Penamid.Penamid
		Senuid.Senuid.Senuid.Senuid
		Setnid.Setnid.Setnid.Setnid
		Arganid.Arganid.Arganid.Arganid
		Psammid.Psammid.Psammid.Psammid
		Philothid.Philothid.Philothid.Philothid
		Timolid.Timolid.Timolid.Timolid
		Zosid.Zosid.Zosid.Zosid
		Phileid.Phileid.Phileid.Phileid
		Nicodemid.Nicodemid.Nicodemid.Nicodemid
		Nefarid.Nefarid.Nefarid.Nefarid
		Bastid.Bastid.Bastid.Bastid
		Xeneid.Xeneid.Xeneid.Xeneid
		Naravid.Naravid.Naravid.Naravid
		Ezanid.Ezanid.Ezanid.Ezanid
		Ahmid.Ahmid.Ahmid.Ahmid
		Alarus.Alara.Alarid.Alarid
		Harsiotes.Harsiotes.Harsiotid.Harsiotid
		Henus.Hena.Henid.Henid
		Ibus.Iba.Ibid.Ibid
		Khabbash.Khabbash.Khabbashid.Khabbashid	
		Amasus.Amasa.Amasid.Amasid
		Nectanebo.Nectanebo.Nectanebid.Nectanebid
		Petubast.Petubast.Petubastes.Petubastid
		Djedhid.Djedhid.Djedhid.Djedhid
		Tachys.Tachya.Tachyes.Tachyid
		Mago.Mago.Mago.Magid	
	}
	culture = {
		maganite = {}
	}
	
	barbarian_names = { 
		maganite_barb
	}
	
	graphical_culture = numidian_gfx
	ethnicities = {
		10 = berber
	}
}