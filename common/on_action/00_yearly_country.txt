﻿yearly_country_pulse =
{
	on_actions = {
		delay = { days = { 30 335 } }
		main_event_pulse_country
		delay = { days = { 1 364 } }
		yearly_gambling_pulse
		delay = { days = { 100 200 } }
		yearly_disaster_pulse
		delay = { days = 150 } #Do not adjust
		yearly_subject_pulse
		prominent_actors_setup
		delay = { days = 150 }
		claims_pulse
		delay = { days = { 1 364 } }
		yearly_comet_pulse
		delay = { days = { 1 364 } }
		disloyal_powerful_characters_pulse
		delay = { days = { 1 364 } }
		yearly_bad_advisor_pulse
		delay = { days = { 1 364 } }
		yearly_good_advisor_pulse
		delay = { days = { 1 364 } }
		great_work_yearly_pulse
		delay = { days = { 1 364 } }
		great_work_five_yearly_pulse
		delay = { days = { 1 364 } }
		ba_bronze_pulse
		delay = { days = { 1 364 } }
		no_great_work_pulse
		delay = { days = { 1 364 } }
		ba_tech_pulse
		delay = { days = { 1 364 } }		
		great_work_ai_construction_yearly_pulse
		delay = { days = { 150 250 } }
		great_work_misc_yearly_pulse
	}
	
	effect = {
		if = {
			limit = { NOT = { has_global_variable = stormbringer } }
			random_country = {
				limit = {
					num_of_cities >= 25
					war = no
				}
				trigger_event = {
					id = storm.1 #Seas
					days = { 10 60 }
				}
				trigger_event = {
					id = storm.11 #Deserts
					days = { 10 60 }
				}
				trigger_event = {
					id = storm.21 #Snowstorms
				}
				trigger_event = {
					id = storm.25
					days = 2
				}				
			}
			set_global_variable = stormbringer
		}
		if = {
			limit = { NOT = { has_global_variable = egyptian_drought_removed } }
			random_country = {
				limit = {
					num_of_cities >= 2
					war = no
				}
				trigger_event = {
					on_action = ba_egyptian_drought_removed
				}
				trigger_event = {
					on_action = ba_egyptian_drought_lessens
				}			
			}
			set_global_variable = stormbringer
		}
				
	}
	
	random_events = {
		10 = succession_crisis.4
		30 = country_diplomacy.39
		110 = 0
	}
	events = {
		country_diplomacy.40
		gw_effects.1	# Great Works martial training
		gw_effects.2	# Great Works finesse training
		gw_effects.3	# Great Works charisma training
		gw_effects.4	# Great Works zeal training
		ba_egypt_flavor.1
		ba_egypt_flavor.2
		expand.10
	}
}
