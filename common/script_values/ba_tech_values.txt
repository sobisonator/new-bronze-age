﻿civ_tech_tech_diff_svalue = {
	value = civic_tech
	max = {
		value = civic_tech
		max = {
			value = military_tech
			max = {
				value = oratory_tech
				max = religious_tech
			}
		}
	}
	add = {
		value = var:civ_tech
		multiply = -1
	}
}
civ_tech_tech_diff_UI_svalue = {
	value = civ_tech_tech_diff_svalue
	multiply = 2
}
civ_tech_1_compare_svalue = {
	value = var:civ_tech
	add = 0.5
}
civ_tech_2_compare_svalue = {
	value = var:civ_tech
	add = 1
}
civ_tech_3_compare_svalue = {
	value = var:civ_tech
	add = 1.5
}