﻿country_decisions = {
	
	# Form Phrygia
	form_one_phrygia = {
		potential = {
			NOT = {
				tag = 1PY
			}
			NOR = {
				is_tier_1_formable_trigger = yes
				is_tier_2_formable_trigger = yes
				is_endgame_tag_trigger = yes
			}
			country_culture_group = bryges # Phrygian
		}
		
		allow = {
			custom_tooltip = {
				text = formable_not_phrygia_exists
				NOT = {
					any_country = {
						tag = 1PY
					}
				}
			}
			can_form_nation_trigger = yes
			OR = {
				custom_tooltip = {
					text = formable_phrygia_asia_minor_condition
					any_owned_province = {
						OR = {
							is_in_region = wilusa_region
							is_in_region = adramyttenos_bay_region
							is_in_region = seha_region
							is_in_region = warmala_region
							is_in_region = lelegia_region
							is_in_region = kaystros_region
							is_in_region = maiandros_region
							is_in_region = karkisa_region
							is_in_region = salbake_region
							is_in_region = north_lukka
							is_in_region = south_lukka
						}
						count >= 50
					}
				}
				num_of_cities >= 100
			}
		}
		
		ai_allow = {
			OR = {
				custom_tooltip = {
					text = formable_phrygia_asia_minor_condition
					any_owned_province = {
						OR = {
							is_in_region = wilusa_region
							is_in_region = adramyttenos_bay_region
							is_in_region = seha_region
							is_in_region = warmala_region
							is_in_region = lelegia_region
							is_in_region = kaystros_region
							is_in_region = maiandros_region
							is_in_region = karkisa_region
							is_in_region = salbake_region
							is_in_region = north_lukka
							is_in_region = south_lukka
						}
						count >= 50
					}
				}
				num_of_cities >= 100
			}
		}
		
		effect = {
			change_country_name = "ONE_PHRYGIA_NAME"
			hidden_effect = {
				change_country_adjective = "ONE_PHRYGIA_ADJECTIVE"
				change_country_tag = 1PY
				change_country_color = phrygia_red
				change_country_flag = PRY
			}
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_medium_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			add_2_free_province_investments = yes
			change_government = despotic_monarchy
			switch_government_type_event_clearup_effect = yes
		}
		
		ai_will_do = {
			base = 1
		}
	}
} 

