﻿country_decisions = {

	# Form Lukka
	form_one_lukka = { 
		
		potential = { 
			num_of_cities >= 1 
			NOT = {
				tag = 1LK
			}
			NOR = { 
				is_endgame_tag_trigger = yes 
				is_tier_2_formable_trigger = yes #Tier 2 so that Arzawa or Assuwa league can be a larger formable, depending on disputed history
			}
			capital_scope = { #majority lukkan regions for simplicity
				OR = {
					is_in_region = north_lukka_region
					is_in_region = south_lukka_region
					is_in_area = kaunos_area
				}
			}
			primary_culture = lukkan #only lukkan culture countries
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 7
					province_id = 1333
					province_id = 1256
					province_id = 3007
					province_id = 2937
					province_id = 2950
					province_id = 3320
					province_id = 2965
					province_id = 2972
					province_id = 2988
					province_id = 2995
					province_id = 1889
					province_id = 2495
					province_id = 3328
					province_id = 3339
					province_id = 3033
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1LK
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 7 #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 1333
			owns_or_subject_owns = 1256
			owns_or_subject_owns = 3007
			owns_or_subject_owns = 2937
			owns_or_subject_owns = 2950
			owns_or_subject_owns = 3320
			owns_or_subject_owns = 2965
			owns_or_subject_owns = 2972
			owns_or_subject_owns = 2988
			owns_or_subject_owns = 2995
			owns_or_subject_owns = 1889
			owns_or_subject_owns = 2495
			owns_or_subject_owns = 3328
			owns_or_subject_owns = 3339
			owns_or_subject_owns = 3033
		}
		
		ai_allow = {
			owns_or_subject_owns = 7 #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 1333
			owns_or_subject_owns = 1256
			owns_or_subject_owns = 3007
			owns_or_subject_owns = 2937
			owns_or_subject_owns = 2950
			owns_or_subject_owns = 3320
			owns_or_subject_owns = 2965
			owns_or_subject_owns = 2972
			owns_or_subject_owns = 2988
			owns_or_subject_owns = 2995
			owns_or_subject_owns = 1889
			owns_or_subject_owns = 2495
			owns_or_subject_owns = 3328
			owns_or_subject_owns = 3339
			owns_or_subject_owns = 3033
		}
		
		effect = {
			change_country_name = "ONE_LUKKA_NAME"
			add_4_free_province_investments = yes 
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_medium_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_LUKKA_ADJECTIVE"
				change_country_tag = 1LK
				change_country_color = numidia_yellow  #Placeholder until a better idea is put forth
				#change_country_flag = Lukka_FLAG #Haven't a clue what kind of flag this would use
				every_province = {
					limit = {
						OR = {
							is_in_region = north_lukka_region
							is_in_region = south_lukka_region
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
}