﻿country_decisions = {

	# Form Pelasgia
	form_one_peloponnesos = { 
		
		potential = { 
			num_of_cities >= 30
			NOT = {
				tag = 1PP
			}
			NOR = { 
				is_endgame_tag_trigger = yes 
				is_tier_2_formable_trigger = yes
			}
			capital_scope = {
				OR = {
					is_in_region = west_peloponessos_region
					is_in_region = east_peloponessos_region
					is_in_area = korinthos_area
					province_id = 325 #Northwestern Athens
					province_id = 251
					province_id = 250
					province_id = 252
				}
				NOT = {
					province_id = 124 # excludes Kythera
					province_id = 122
					province_id = 123
					province_id = 226 # some island off argolid
					province_id = 629
					province_id = 248 #excludes Salamis
					province_id = 249
				}
			}				 
			NOT = { primary_culture = helladic }
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 4	#lakonia
					province_id = 11
					province_id = 19
					province_id = 18
					province_id = 125
					province_id = 118
					province_id = 113
					province_id = 50	#messenia
					province_id = 108
					province_id = 55
					province_id = 28
					province_id = 25
					province_id = 162	#arkadia
					province_id = 137
					province_id = 143
					province_id = 146
					province_id = 154
					province_id = 157
					province_id = 68	#elis
					province_id = 1497
					province_id = 167
					province_id = 169
					province_id = 181
					province_id = 177
					province_id = 185	#achaia
					province_id = 191
					province_id = 149
					province_id = 196
					province_id = 199
					province_id = 129	#argolis
					province_id = 216
					province_id = 218
					province_id = 223
					province_id = 227
					province_id = 209	#korinthia
					province_id = 207
					province_id = 234
					province_id = 239
					province_id = 246
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1TS
					}
				}
			}
			can_form_nation_trigger = yes 
			OR = {
				any_owned_province = {
					OR = {
						is_in_region = west_peloponessos_region
						is_in_region = east_peloponessos_region
						is_in_area = korinthos_area
					}
					NOT = {
						province_id = 124 # excludes Kythera
						province_id = 122
						province_id = 123
						province_id = 226 # some island off argolid
						province_id = 629
						province_id = 248 #excludes Salamis
						province_id = 249
					}
					count >= 120
				}
			}
		}
		
		ai_allow = { #Doesn't have to be owned directly, vassal owning it is fine
			OR = {
				any_owned_province = {
					OR = {
						is_in_region = west_peloponessos_region
						is_in_region = east_peloponessos_region
						is_in_area = korinthos_area
					}
					NOT = {
						province_id = 124 # excludes Kythera
						province_id = 122
						province_id = 123
						province_id = 226 # some island off argolid
						province_id = 629
						province_id = 248 #excludes Salamis
						province_id = 249
					}
					count >= 120
				}
			}
		}
		
		effect = {
			change_country_name = "ONE_PELOPONNESOS_NAME"
			add_2_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_medium_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_PELASGIA_ADJECTIVE"
				change_country_tag = 1PP
				change_country_color = sun_yellow
				#change_country_flag = 
				every_province = {
					limit = {
						OR = {
							is_in_region = west_peloponessos_region
							is_in_region = east_peloponessos_region
							is_in_area = korinthos_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
} 