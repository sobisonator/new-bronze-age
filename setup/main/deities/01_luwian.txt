deity_manager = {
	deities_database = { ### KEYS 1100-1199 ###
		# Luwian Deities
		1100 = {
			key = omen_tarhunz
			deity = deity_tarhunz
		}
		1101 = {
			key = omen_tiwad
			deity = deity_tiwad
		}
		1102 = {
			key = omen_kamrushipa
			deity = deity_kamrushipa
		}
		1103 = {
			key = omen_arma
			deity = deity_arma
		}
		1104 = {
			key = omen_runtiya
			deity = deity_runtiya
		}
		1105 = {
			key = omen_gulza
			deity = deity_gulza
		}
		1106 = {
			key = omen_lulimi
			deity = deity_lulimi
		}
		1107 = {
			key = omen_kurshash
			deity = deity_kurshash
		}
		1108 = {
			key = omen_yarri
			deity = deity_yarri
		}
		1109 = {
			key = omen_shanta
			deity = deity_shanta
		}
		1110 = {
			key = omen_innarawantesh
			deity = deity_innarawantesh
		}
		1111 = {
			key = omen_ala
			deity = deity_ala
		}
	}
}