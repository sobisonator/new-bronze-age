deity_manager = {
	deities_database = { ### KEYS 1350 - 1399 ###
		1350 = {
			key = omen_gilgamesh
			deity = deity_gilgamesh
			deify_ruler = 15
			deify_ruler_permanent = yes
			is_universal_deity = yes
		}
		1351 = {
			key = omen_imhotep
			deity = deity_imhotep
			deify_ruler = 16
			deify_ruler_permanent = yes
			is_universal_deity = yes
		}
		1352 = {
			key = omen_kubau
			deity = deity_kubau
			deify_ruler = 17
			deify_ruler_permanent = yes
			is_universal_deity = yes
		}
		1353 = {
			key = omen_sargon
			deity = deity_sargon
			deify_ruler = 18
			deify_ruler_permanent = yes
			is_universal_deity = yes
		}
	}
} 