﻿1={ #Amykles
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2={ #Skoura
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

4={ #Menelaion
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cloth"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

5={ #Loggastra
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6={ #Vresthena
	terrain="mountain_valley"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="base_metals"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

8={ #Gorani
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

11={ #Gythion
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

12={ #Passavas
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
}

15={ #Akriai
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cloth"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

17={ #Taenaron
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

18={ #Grammousa
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cloth"
	civilization_value=6
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
}

19={ #Kaseno
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

22={ #Areópoli
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=3
	}
}

24={ #Élos
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

25={ #Leuktro
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=4
	}
}

29={ #Kitriaí
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

32={ #Aecernia
	terrain="mountain"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="stone"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

111={ #Kremasti
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

112={ #Richea
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="stone"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=4
	}
}

113={ #Asopos
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

114={ #Elika
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

115={ #Sykéa
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

118={ #Pavlopetri
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

120={ #Boiai
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

121={ #Epitrillium
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

122={ #Skandeia
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	slaves={
		amount=1
	}
	tribesmen={
		amount=5
	}
}

123={ #Kastri
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

124={ #Kythera
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

125={ #Epidauros Limera
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

126={ #Zarax
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="earthware"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

127={ #Kypantha
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

128={ #Prasiae
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

129={ #Kosmas
	terrain="deep_forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	slaves={
		amount=1
	}
	tribesmen={
		amount=4
	}
}

130={ #Leonídio
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

131={ #Methana
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="stone"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

132={ #Sellasia
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="honey"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
}

135={ #Asea
	terrain="deep_forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

136={ #Silimna
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="base_metals"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

137={ #Tegea
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

140={ #Varson
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wild_game"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

141={ #Mantineia
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

143={ #Orchomenos
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

144={ #Piana
	terrain="deep_forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fur"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

159={ #Perivolia
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="stone"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

200={ #Aegira
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="earthware"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

201={ #Lavka
	terrain="mountain"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fur"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

202={ #Rethi
	terrain="mountain_valley"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=4
	}
	holy_site = omen_hermahas
}

203={ #Xylokastro
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

204={ #Pasion
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

205={ #Kalianoi
	terrain="mountain"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=5
	}
}

206={ #Gimno
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="precious_metals"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	slaves={
		amount=1
	}
	tribesmen={
		amount=4
	}
}

207={ #Titani
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="base_metals"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	slaves={
		amount=1
	}
	tribesmen={
		amount=4
	}
}

208={ #Soulinari
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

209={ #Sykyon
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

210={ #Nemea
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

211={ #Mykene
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

212={ #Lerna
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

214={ #Agrilitsa
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

215={ #Akova
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="earthware"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

216={ #Argos
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

217={ #Midea
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

218={ #Tiryns
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cloth"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=3
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

219={ #Thyrea
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

220={ #Drepanon
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

221={ #Asini
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
}

222={ #Salanti
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

223={ #Kosta
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

224={ #Ermione
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

225={ #Metochi
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="hemp"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

226={ #Vlichos
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

227={ #Troizena
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=13
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

228={ #Epidauros
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="hemp"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

229={ #Methana
	terrain="plains"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

231={ #Dhimaina
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

232={ #Arakhnaion
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

629={ #Aigina
	terrain="hills"
	culture="kranaoi"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1183={ #Glyppia
	terrain="farmland"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="earthware"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
}

1206={ #Antikythera
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2504={ #Hysiai
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2505={ #Neris
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2506={ #Soreno
	terrain="hills"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="stone"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

5550={ #Rateus
	terrain="forest"
	culture="helladic"
	religion="aegean_religion"
	trade_goods="fur"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

